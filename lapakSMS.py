#This one work with MODEM GSM too
#Very neat

import os
from Tkinter import *
import tkFileDialog
import re
import serial
import time
from collections import OrderedDict
import shutil
appname = "LapakSMS"
companyName = "by LapakLab.com"
global locNum, limitedchar, diroutput, txtdefault
locNum = "0361"
limitedchar = 160
txtdefault = "contentsms.txt"
diroutput = "C:\SMSlog"
    
def cfile():
    #root.withdraw()
    global filename, newmsg, dst
    filename = tkFileDialog.askopenfilename()
    if os.path.isfile(filename):
        hplist=[]
        for line in open(filename,'r'):
            #print line,
            hplist.append(line)
        newmsg = "Jumlah nomer yang akan dikirimi sebanyak %s" % len(hplist)
        print newmsg
        basefile = os.path.splitext(os.path.basename(str(filename)))[0]
        if not os.path.exists(diroutput):
            os.makedirs(diroutput)
        dst = diroutput + "/" + basefile + ".txt"
        shutil.copy(filename, dst)
        #entry_contents.set(filename)
        
    else:
        print 'No file chosen'
    #raw_input('\nReady, push Enter')
    ent.delete(0, END)
    ent.insert(0, str(filename))
    v.set(newmsg)
    print newmsg
    #root.deiconify()
    print "OK!"
    filterio()

def filterio():
    global myfile
    myfile = os.path.splitext(os.path.basename(str(dst)))[0]
    #myfile = os.path.splitext(str(filename))[0]
    #myfile = 'test-hp'
    print myfile
    filetxt = diroutput + "/" + myfile+'.txt'
    non_blank_count = 0
    with open(filetxt) as infp:
        for line in infp:
           if line.strip():
              non_blank_count += 1

    print 'number of non-blank lines found %d' % non_blank_count

    f = open(diroutput + "/" + myfile+'-to-send.txt','w')
    #f.write('--start--\n')
    # python will convert \n to os.linesep


    with open(filetxt) as fin:
    
        lines = (line.rstrip().lstrip() for line in fin)
    
        unique_lines = OrderedDict.fromkeys( (line for line in lines if line) )
    k = unique_lines.keys()     
    print "unik nomer hp: %d" % len(unique_lines.keys())

    """proses filter
    - duplicate line remove [x]
    - blank line remove [x]
    - remove len < 10 [x]
    - remove len > 12 [x]
    - first string must 08 or 0361 [x]
    - if 0361 then after must be len ==7 (untuk flexi) [x]
    - else remove [x]
    - contain one minimum string then remove [x]
    - print count will be process
    - write to send [x]
    """
    #int(x.group()) for x in re.finditer(r'\d+', string1)
    print 'was %d' % len(k)
    k = [ x for x in k if (len(x)>= 10)]
    k = [ x for x in k if (len(x)< 13)]
    k = [ x for x in k if ((x[:2] =='08') or (x[:4] =='0361'))]

    i = 0
    n = len(k)
    print 'before process %d' % n
    while i < n:
        element = k[i]
        #do_action(element)
        if ((element[:4] ==locNum) and (len(element)==10)):
            del k[i]
            n = n - 1
        else:
            i = i + 1

    """cek integer"""

    print 'now %d' % len(k)

    for i in k:
        try:
            p = int(i)
            print '%s -- %s' % (i, len(i),)
            f.write(i+'\n')
        except ValueError:
            print '%s -- %s Not exactly HP number' % (i, len(i),)
    f.close()  
    with open(diroutput + "/" +myfile+'-to-send.txt') as fin:
        lines = sum(1 for line in fin)
    print 'will be process %d' % lines
    v.set(newmsg+" (valid %d)" % lines)
    

    #print 'now %d' % len(k)
    #f.write('--end--\n')


def count(event):
    current = len(post_tweet.get(1.0, "end-1c"))
    remaining = 160-current
    char_count.configure(text="%s karakter tersisa" % remaining)

def sendsms():
    """
    sms.py - Used to send txt messages.
    send max 4000 sms / day.
    duration 6,9 hours

    fitur:
    - cek karakter <160
    - cek bad karakter reject
    - choose file txt
    - save report to txt log
    - auto configure column dengan excel

    """

    global bodymsg
    bodymsg = post_tweet.get(1.0, END)
    myfilesend = myfile+'-to-send' #hp-1996-191013 #hp-696-191013
    textfile = diroutput + "/" + myfilesend+'.txt'
    logfile = diroutput + "/" + myfilesend+'-log.txt'
    print textfile
    with open(textfile, 'r') as f:
        myNumber = [line.strip() for line in f]

    print myNumber
    print "Found ports:"
    global suse
    lsuse = []
    for n,s in scan():
        print "(%d) %s" % (n,s)
        lsuse.append(s)
        

    try :
        suse = lsuse[1]
    except:
        print "Modem tidak terhubung di USB port dengan benar. Periksa pemasangan port modem anda."

    print "use now %s" % suse

    global bdy
    f = open(txtdefault, 'w')
    f.write(bodymsg)
    f.close()

    with open(txtdefault) as txtftp:
        for line in txtftp:
           if line.strip():
              bdy = line
    print bdy
    
    f = open(logfile,'w')
    t = 0
    j = 0
    u = len(myNumber)
    if (cfound > 0):
        print "There are %d port available" % cfound
        root.withdraw()
        for i in myNumber:
            #print "------#start#------" 
            start_time = time.time()
            #print "start %s" % start_time
            sms = TextMessage(i, bodymsg)
            sms.connectPhone()
            sms.sendMessage()
            #sms.readLastLine()
            #sms.receiving()
            t = t + 1
            print t, " of ", u, " sent to ", i.ljust(14), "takes ", time.time() - start_time, "seconds"
            #print "end %s" % time.time()
            #f.write("%s  >> OK, start %s takes %s seconds until %s \n" % (i, start_time, (time.time() - start_time), time.time()) )
            f.write("%s of %s sent to %s  >> OK, takes %s seconds\n" % (t, u, i.ljust(14), (time.time() - start_time)) )

            time.sleep(4)
            #reset value of j
            j = j + 1
            if (j == 10):
                time.sleep(10)
                print 'pause for 10 seconds more...'
                j = 0
            #print "------#end#------\n" 
            sms.disconnectPhone()
        root.deiconify()
    else:
        print "Nothing executed"

    print "Completed sent"
    f.write("Completed")
    f.close()


def scan():
    global cfound
    # scan for available ports. return a list of tuples (num, name)
    available = []
    for i in range(256):
        try:
            s = serial.Serial(i)
            available.append( (i, s.portstr))
            s.close()
        except serial.SerialException:
            pass
    cfound = len(available)
     
    return available



class TextMessage:
    def __init__(self, recipient="081236000196", message="Not Set"):
        self.recipient = recipient
        self.content = txtdefault
 
    def setRecipient(self, number):
        self.recipient = number
 
    def setContent(self, message):
        self.content = message
 
    def connectPhone(self):
        self.ser = serial.Serial(suse, 115200, timeout=1)
        time.sleep(1)
 
    def sendMessage(self):
        self.ser.write('AT\r')
        time.sleep(1)
        #delete inbox
        self.ser.write('AT+CMGD=1,4\r')
        time.sleep(3)
        self.ser.write('AT+CMGF=1\r')
        time.sleep(1)
        self.ser.write('''AT+CMGS="''' + self.recipient + '''"\r''')
        time.sleep(1)
        self.ser.write(bdy + '\r\n')
        time.sleep(1)
        self.ser.write(chr(26))
        time.sleep(1)

        
    def disconnectPhone(self):
        self.ser.close()

    def readLastLine(self):
        last_data=''
        while True:
            data=self.ser.readline()
            if data!='':
                last_data=data
            else:
                return last_data
            print last_data

    def receiving(self):
        global last_received
        buffer = ''
        while True:
            buffer = buffer + self.ser.read(self.ser.inWaiting())
            if '\n' in buffer:
                lines = buffer.split('\n') # Guaranteed to have at least 2 entries
                last_received = lines[-2]
                #If the Arduino sends lots of empty lines, you'll lose the
                #last filled line, so you could make the above statement conditional
                #like so: if lines[-2]: last_received = lines[-2]
                buffer = lines[-1]
                print "status %s" % buffer
                print "last status %s" % last_received


global ent, v
root = Tk()
root.minsize(300,300)
root.geometry("700x400")
root.title('%s - %s' % (appname, companyName,))
root.wm_iconbitmap('sms.ico')



myC = Frame(root)  ### (1)
myC.pack()         ### (2)
aForm = LabelFrame(root, text="Import Text File", padx=5, pady=5)
#aForm.pack(expand = 1, pady = 10, padx = 10)
aForm.pack(side=TOP, expand = 1, pady = 5, padx = 5)
#side=LEFT, padx=5
message = "Pilih sumber daftar nomer HP yang akan dikirimi."
Label(aForm, text=message).pack()
btnA = Button(aForm, text='Choose File', command=cfile)
btnA.pack(side=LEFT, padx=5)


ent = Entry(aForm, font=("Arial", "9"))
ent.pack(side=RIGHT, expand=YES, fill=X)

msg = Frame(root)
msg.pack()
Label(msg).pack()
v = StringVar()
went = Label(msg, textvariable=v, font=("Arial", "9"))
went.pack(side=LEFT)



bForm = LabelFrame(root, text="Pesan", padx=5, pady=5)
bForm.pack(side=TOP, expand = 1, pady = 5, padx = 5)
bmsg = "Masukkan pesan"
Label(bForm, text=bmsg).pack()
bodyFrame = Frame(bForm)
bodyFrame.pack(fill=BOTH, expand=1)


post_tweet = Text(bodyFrame, width=50, height=8, font=("Arial", "9"))
bindtags = list(post_tweet.bindtags())
bindtags.insert(2, "custom") # index 1 is where most default bindings live
post_tweet.bindtags(tuple(bindtags))

post_tweet.bind_class("custom", "<Key>", count)
post_tweet.pack()

char_count = Label()
char_count.pack()
Label().pack()

#entbody = Text(bodyFrame, width=50, height=8, font=("Arial", "9"))
#entbody.pack()
btnB = Button(bForm, text='Kirim!', command=sendsms)
btnB.pack(side=LEFT, padx=5)


root.mainloop()
